// Minmax implements Min and Max functions for standard types.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package compare

/////////////////////////////////////////////////////////////////////////
// MinOfPair functions
/////////////////////////////////////////////////////////////////////////

// MinOfPairInt returns the smaller of the two given values.
func MinOfPairInt(v1 int, v2 int) int {
	if v1 < v2 {
		return v1
	}
	return v2
}

// MinOfPairInt8 returns the smaller of the two given values.
func MinOfPairInt8(v1 int8, v2 int8) int8 {
	if v1 < v2 {
		return v1
	}
	return v2
}

// MinOfPairInt16 returns the smaller of the two given values.
func MinOfPairInt16(v1 int16, v2 int16) int16 {
	if v1 < v2 {
		return v1
	}
	return v2
}

// MinOfPairInt32 returns the smaller of the two given values.
func MinOfPairInt32(v1 int32, v2 int32) int32 {
	if v1 < v2 {
		return v1
	}
	return v2
}

// MinOfPairInt64 returns the smaller of the two given values.
func MinOfPairInt64(v1 int64, v2 int64) int64 {
	if v1 < v2 {
		return v1
	}
	return v2
}

// MinOfPairUint returns the smaller of the two given values.
func MinOfPairUint(v1 uint, v2 uint) uint {
	if v1 < v2 {
		return v1
	}
	return v2
}

// MinOfPairUint8 returns the smaller of the two given values.
func MinOfPairUint8(v1 uint8, v2 uint8) uint8 {
	if v1 < v2 {
		return v1
	}
	return v2
}

// MinOfPairUint16 returns the smaller of the two given values.
func MinOfPairUint16(v1 uint16, v2 uint16) uint16 {
	if v1 < v2 {
		return v1
	}
	return v2
}

// MinOfPairUint32 returns the smaller of the two given values.
func MinOfPairUint32(v1 uint32, v2 uint32) uint32 {
	if v1 < v2 {
		return v1
	}
	return v2
}

// MinOfPairUint64 returns the smaller of the two given values.
func MinOfPairUint64(v1 uint64, v2 uint64) uint64 {
	if v1 < v2 {
		return v1
	}
	return v2
}

/////////////////////////////////////////////////////////////////////////
// MaxOfPair functions
/////////////////////////////////////////////////////////////////////////

// MaxOfPairInt returns the larger of the two given values.
func MaxOfPairInt(v1 int, v2 int) int {
	if v1 > v2 {
		return v1
	}
	return v2
}

// MaxOfPairInt8 returns the larger of the two given values.
func MaxOfPairInt8(v1 int8, v2 int8) int8 {
	if v1 > v2 {
		return v1
	}
	return v2
}

// MaxOfPairInt16 returns the larger of the two given values.
func MaxOfPairInt16(v1 int16, v2 int16) int16 {
	if v1 > v2 {
		return v1
	}
	return v2
}

// MaxOfPairInt32 returns the larger of the two given values.
func MaxOfPairInt32(v1 int32, v2 int32) int32 {
	if v1 > v2 {
		return v1
	}
	return v2
}

// MaxOfPairInt64 returns the larger of the two given values.
func MaxOfPairInt64(v1 int64, v2 int64) int64 {
	if v1 > v2 {
		return v1
	}
	return v2
}

// MaxOfPairUint returns the larger of the two given values.
func MaxOfPairUint(v1 uint, v2 uint) uint {
	if v1 > v2 {
		return v1
	}
	return v2
}

// MaxOfPairUint8 returns the larger of the two given values.
func MaxOfPairUint8(v1 uint8, v2 uint8) uint8 {
	if v1 > v2 {
		return v1
	}
	return v2
}

// MaxOfPairUint16 returns the larger of the two given values.
func MaxOfPairUint16(v1 uint16, v2 uint16) uint16 {
	if v1 > v2 {
		return v1
	}
	return v2
}

// MaxOfPairUint32 returns the larger of the two given values.
func MaxOfPairUint32(v1 uint32, v2 uint32) uint32 {
	if v1 > v2 {
		return v1
	}
	return v2
}

// MaxOfPairUint64 returns the larger of the two given values.
func MaxOfPairUint64(v1 uint64, v2 uint64) uint64 {
	if v1 > v2 {
		return v1
	}
	return v2
}

/////////////////////////////////////////////////////////////////////////
// Min functions
/////////////////////////////////////////////////////////////////////////

// MinInt returns the smallest value in the slice S. If S is empty then 0 is returned.
func MinInt(S ...int) int {
	if len(S) == 0 {
		return 0
	}
	min := S[0]
	for _, s := range S[1:] {
		if s < min {
			min = s
		}
	}
	return min
}

// MinInt8 returns the smallest value in the slice S. If S is empty then 0 is returned.
func MinInt8(S ...int8) int8 {
	if len(S) == 0 {
		return 0
	}
	min := S[0]
	for _, s := range S[1:] {
		if s < min {
			min = s
		}
	}
	return min
}

// MinInt16 returns the smallest value in the slice S. If S is empty then 0 is returned.
func MinInt16(S ...int16) int16 {
	if len(S) == 0 {
		return 0
	}
	min := S[0]
	for _, s := range S[1:] {
		if s < min {
			min = s
		}
	}
	return min
}

// MinInt32 returns the smallest value in the slice S. If S is empty then 0 is returned.
func MinInt32(S ...int32) int32 {
	if len(S) == 0 {
		return 0
	}
	min := S[0]
	for _, s := range S[1:] {
		if s < min {
			min = s
		}
	}
	return min
}

// MinInt64 returns the smallest value in the slice S. If S is empty then 0 is returned.
func MinInt64(S ...int64) int64 {
	if len(S) == 0 {
		return 0
	}
	min := S[0]
	for _, s := range S[1:] {
		if s < min {
			min = s
		}
	}
	return min
}

// MinUint returns the smallest value in the slice S. If S is empty then 0 is returned.
func MinUint(S ...uint) uint {
	if len(S) == 0 {
		return 0
	}
	min := S[0]
	for _, s := range S[1:] {
		if s < min {
			min = s
		}
	}
	return min
}

// MinUint8 returns the smallest value in the slice S. If S is empty then 0 is returned.
func MinUint8(S ...uint8) uint8 {
	if len(S) == 0 {
		return 0
	}
	min := S[0]
	for _, s := range S[1:] {
		if s < min {
			min = s
		}
	}
	return min
}

// MinUint16 returns the smallest value in the slice S. If S is empty then 0 is returned.
func MinUint16(S ...uint16) uint16 {
	if len(S) == 0 {
		return 0
	}
	min := S[0]
	for _, s := range S[1:] {
		if s < min {
			min = s
		}
	}
	return min
}

// MinUint32 returns the smallest value in the slice S. If S is empty then 0 is returned.
func MinUint32(S ...uint32) uint32 {
	if len(S) == 0 {
		return 0
	}
	min := S[0]
	for _, s := range S[1:] {
		if s < min {
			min = s
		}
	}
	return min
}

// MinUint64 returns the smallest value in the slice S. If S is empty then 0 is returned.
func MinUint64(S ...uint64) uint64 {
	if len(S) == 0 {
		return 0
	}
	min := S[0]
	for _, s := range S[1:] {
		if s < min {
			min = s
		}
	}
	return min
}

/////////////////////////////////////////////////////////////////////////
// Max functions
/////////////////////////////////////////////////////////////////////////

// MaxInt returns the largest value in the slice S. If S is empty then 0 is returned.
func MaxInt(S ...int) int {
	if len(S) == 0 {
		return 0
	}
	max := S[0]
	for _, s := range S[1:] {
		if s > max {
			max = s
		}
	}
	return max
}

// MaxInt8 returns the largest value in the slice S. If S is empty then 0 is returned.
func MaxInt8(S ...int8) int8 {
	if len(S) == 0 {
		return 0
	}
	max := S[0]
	for _, s := range S[1:] {
		if s > max {
			max = s
		}
	}
	return max
}

// MaxInt16 returns the largest value in the slice S. If S is empty then 0 is returned.
func MaxInt16(S ...int16) int16 {
	if len(S) == 0 {
		return 0
	}
	max := S[0]
	for _, s := range S[1:] {
		if s > max {
			max = s
		}
	}
	return max
}

// MaxInt32 returns the largest value in the slice S. If S is empty then 0 is returned.
func MaxInt32(S ...int32) int32 {
	if len(S) == 0 {
		return 0
	}
	max := S[0]
	for _, s := range S[1:] {
		if s > max {
			max = s
		}
	}
	return max
}

// MaxInt64 returns the largest value in the slice S. If S is empty then 0 is returned.
func MaxInt64(S ...int64) int64 {
	if len(S) == 0 {
		return 0
	}
	max := S[0]
	for _, s := range S[1:] {
		if s > max {
			max = s
		}
	}
	return max
}

// MaxUint returns the largest value in the slice S. If S is empty then 0 is returned.
func MaxUint(S ...uint) uint {
	if len(S) == 0 {
		return 0
	}
	max := S[0]
	for _, s := range S[1:] {
		if s > max {
			max = s
		}
	}
	return max
}

// MaxUint8 returns the largest value in the slice S. If S is empty then 0 is returned.
func MaxUint8(S ...uint8) uint8 {
	if len(S) == 0 {
		return 0
	}
	max := S[0]
	for _, s := range S[1:] {
		if s > max {
			max = s
		}
	}
	return max
}

// MaxUint16 returns the largest value in the slice S. If S is empty then 0 is returned.
func MaxUint16(S ...uint16) uint16 {
	if len(S) == 0 {
		return 0
	}
	max := S[0]
	for _, s := range S[1:] {
		if s > max {
			max = s
		}
	}
	return max
}

// MaxUint32 returns the largest value in the slice S. If S is empty then 0 is returned.
func MaxUint32(S ...uint32) uint32 {
	if len(S) == 0 {
		return 0
	}
	max := S[0]
	for _, s := range S[1:] {
		if s > max {
			max = s
		}
	}
	return max
}

// MaxUint64 returns the largest value in the slice S. If S is empty then 0 is returned.
func MaxUint64(S ...uint64) uint64 {
	if len(S) == 0 {
		return 0
	}
	max := S[0]
	for _, s := range S[1:] {
		if s > max {
			max = s
		}
	}
	return max
}

/////////////////////////////////////////////////////////////////////////
// MinAndIndex functions
/////////////////////////////////////////////////////////////////////////

// MinAndIndexInt returns the smallest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MinAndIndexInt(S ...int) (int, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	min := S[0]
	for i, s := range S[1:] {
		if s < min {
			min = s
			idx = i + 1
		}
	}
	return min, idx
}

// MinAndIndexInt8 returns the smallest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MinAndIndexInt8(S ...int8) (int8, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	min := S[0]
	for i, s := range S[1:] {
		if s < min {
			min = s
			idx = i + 1
		}
	}
	return min, idx
}

// MinAndIndexInt16 returns the smallest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MinAndIndexInt16(S ...int16) (int16, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	min := S[0]
	for i, s := range S[1:] {
		if s < min {
			min = s
			idx = i + 1
		}
	}
	return min, idx
}

// MinAndIndexInt32 returns the smallest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MinAndIndexInt32(S ...int32) (int32, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	min := S[0]
	for i, s := range S[1:] {
		if s < min {
			min = s
			idx = i + 1
		}
	}
	return min, idx
}

// MinAndIndexInt64 returns the smallest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MinAndIndexInt64(S ...int64) (int64, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	min := S[0]
	for i, s := range S[1:] {
		if s < min {
			min = s
			idx = i + 1
		}
	}
	return min, idx
}

// MinAndIndexUint returns the smallest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MinAndIndexUint(S ...uint) (uint, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	min := S[0]
	for i, s := range S[1:] {
		if s < min {
			min = s
			idx = i + 1
		}
	}
	return min, idx
}

// MinAndIndexUint8 returns the smallest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MinAndIndexUint8(S ...uint8) (uint8, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	min := S[0]
	for i, s := range S[1:] {
		if s < min {
			min = s
			idx = i + 1
		}
	}
	return min, idx
}

// MinAndIndexUint16 returns the smallest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MinAndIndexUint16(S ...uint16) (uint16, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	min := S[0]
	for i, s := range S[1:] {
		if s < min {
			min = s
			idx = i + 1
		}
	}
	return min, idx
}

// MinAndIndexUint32 returns the smallest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MinAndIndexUint32(S ...uint32) (uint32, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	min := S[0]
	for i, s := range S[1:] {
		if s < min {
			min = s
			idx = i + 1
		}
	}
	return min, idx
}

// MinAndIndexUint64 returns the smallest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MinAndIndexUint64(S ...uint64) (uint64, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	min := S[0]
	for i, s := range S[1:] {
		if s < min {
			min = s
			idx = i + 1
		}
	}
	return min, idx
}

/////////////////////////////////////////////////////////////////////////
// MaxAndIndex functions
/////////////////////////////////////////////////////////////////////////

// MaxAndIndexInt returns the largest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MaxAndIndexInt(S ...int) (int, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	max := S[0]
	for i, s := range S[1:] {
		if s > max {
			max = s
			idx = i + 1
		}
	}
	return max, idx
}

// MaxAndIndexInt8 returns the largest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MaxAndIndexInt8(S ...int8) (int8, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	max := S[0]
	for i, s := range S[1:] {
		if s > max {
			max = s
			idx = i + 1
		}
	}
	return max, idx
}

// MaxAndIndexInt16 returns the largest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MaxAndIndexInt16(S ...int16) (int16, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	max := S[0]
	for i, s := range S[1:] {
		if s > max {
			max = s
			idx = i + 1
		}
	}
	return max, idx
}

// MaxAndIndexInt32 returns the largest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MaxAndIndexInt32(S ...int32) (int32, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	max := S[0]
	for i, s := range S[1:] {
		if s > max {
			max = s
			idx = i + 1
		}
	}
	return max, idx
}

// MaxAndIndexInt64 returns the largest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MaxAndIndexInt64(S ...int64) (int64, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	max := S[0]
	for i, s := range S[1:] {
		if s > max {
			max = s
			idx = i + 1
		}
	}
	return max, idx
}

// MaxAndIndexUint returns the largest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MaxAndIndexUint(S ...uint) (uint, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	max := S[0]
	for i, s := range S[1:] {
		if s > max {
			max = s
			idx = i + 1
		}
	}
	return max, idx
}

// MaxAndIndexUint8 returns the largest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MaxAndIndexUint8(S ...uint8) (uint8, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	max := S[0]
	for i, s := range S[1:] {
		if s > max {
			max = s
			idx = i + 1
		}
	}
	return max, idx
}

// MaxAndIndexUint16 returns the largest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MaxAndIndexUint16(S ...uint16) (uint16, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	max := S[0]
	for i, s := range S[1:] {
		if s > max {
			max = s
			idx = i + 1
		}
	}
	return max, idx
}

// MaxAndIndexUint32 returns the largest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MaxAndIndexUint32(S ...uint32) (uint32, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	max := S[0]
	for i, s := range S[1:] {
		if s > max {
			max = s
			idx = i + 1
		}
	}
	return max, idx
}

// MaxAndIndexUint64 returns the largest value in the slice S, along with the index of the first occurrence of that value in S. If S is empty then 0 is returned, and the index will be -1.
func MaxAndIndexUint64(S ...uint64) (uint64, int) {
	if len(S) == 0 {
		return 0, -1
	}
	idx := 0
	max := S[0]
	for i, s := range S[1:] {
		if s > max {
			max = s
			idx = i + 1
		}
	}
	return max, idx
}
