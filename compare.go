// Compare defines some useful comparator functions for standard types.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package compare

import (
	"time"
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// String compares the two values v1 and v2. Returns -1 value if v1 < v2, 0 if v1 == v1, and +1 if v1 > v2.
func String(v1 string, v2 string) int {
	if v1 < v2 {
		return -1
	} else if v1 > v2 {
		return 1
	}
	return 0
}

// StringSlice compares the two slices S1 and S2. Returns -1 if S1 < S2, 0 if S1 == S2, and +1 if S1 > S2. Order is grlex: if #S1 < #S2 then S1 < S2, if #S1 > #S2 then S1 > S2, otherwise the first entry such that S1[i] != S2[i] is used to determine the order.
func StringSlice(S1 []string, S2 []string) int {
	if len(S1) < len(S2) {
		return -1
	} else if len(S1) > len(S2) {
		return 1
	}
	for i, s1 := range S1 {
		if s1 < S2[i] {
			return -1
		} else if s1 > S2[i] {
			return 1
		}
	}
	return 0
}

// Bool compares the two values v1 and v2. Returns -1 if v1 < v2, 0 if v1 == v1, and +1 if v1 > v2.
func Bool(v1 bool, v2 bool) int {
	if v1 {
		if v2 {
			return 0
		}
		return 1
	}
	if v2 {
		return -1
	}
	return 0
}

// BoolSlice compares the two slices S1 and S2. Returns -1 if S1 < S2, 0 if S1 == S2, and +1 if S1 > S2. Order is grlex: if #S1 < #S2 then S1 < S2, if #S1 > #S2 then S1 > S2, otherwise the first entry such that S1[i] != S2[i] is used to determine the order.
func BoolSlice(S1 []bool, S2 []bool) int {
	if len(S1) < len(S2) {
		return -1
	} else if len(S1) > len(S2) {
		return 1
	}
	for i, b1 := range S1 {
		if b1 {
			if !S2[i] {
				return 1
			}
		} else if S2[i] {
			return 1
		}
	}
	return 0
}

// Int compares the two values v1 and v2. Returns -1 if v1 < v2, 0 if v1 == v1, and +1 if v1 > v2.
func Int(v1 int, v2 int) int {
	if v1 < v2 {
		return -1
	} else if v1 > v2 {
		return 1
	}
	return 0
}

// Int8 compares the two values v1 and v2. Returns -1 if v1 < v2, 0 if v1 == v1, and +1 if v1 > v2.
func Int8(v1 int8, v2 int8) int {
	if v1 < v2 {
		return -1
	} else if v1 > v2 {
		return 1
	}
	return 0
}

// Int16 compares the two values v1 and v2. Returns -1 if v1 < v2, 0 if v1 == v1, and +1 if v1 > v2.
func Int16(v1 int16, v2 int16) int {
	if v1 < v2 {
		return -1
	} else if v1 > v2 {
		return 1
	}
	return 0
}

// Int32 compares the two values v1 and v2. Returns -1 if v1 < v2, 0 if v1 == v1, and +1 if v1 > v2.
func Int32(v1 int32, v2 int32) int {
	if v1 < v2 {
		return -1
	} else if v1 > v2 {
		return 1
	}
	return 0
}

// Int64 compares the two values v1 and v2. Returns -1 if v1 < v2, 0 if v1 == v1, and +1 if v1 > v2.
func Int64(v1 int64, v2 int64) int {
	if v1 < v2 {
		return -1
	} else if v1 > v2 {
		return 1
	}
	return 0
}

// Uint compares the two values v1 and v2. Returns -1 if v1 < v2, 0 if v1 == v1, and +1 if v1 > v2.
func Uint(v1 uint, v2 uint) int {
	if v1 < v2 {
		return -1
	} else if v1 > v2 {
		return 1
	}
	return 0
}

// Uint8 compares the two values v1 and v2. Returns -1 if v1 < v2, 0 if v1 == v1, and +1 if v1 > v2.
func Uint8(v1 uint8, v2 uint8) int {
	if v1 < v2 {
		return -1
	} else if v1 > v2 {
		return 1
	}
	return 0
}

// Uint16 compares the two values v1 and v2. Returns -1 if v1 < v2, 0 if v1 == v1, and +1 if v1 > v2.
func Uint16(v1 uint16, v2 uint16) int {
	if v1 < v2 {
		return -1
	} else if v1 > v2 {
		return 1
	}
	return 0
}

// Uint32 compares the two values v1 and v2. Returns -1 if v1 < v2, 0 if v1 == v1, and +1 if v1 > v2.
func Uint32(v1 uint32, v2 uint32) int {
	if v1 < v2 {
		return -1
	} else if v1 > v2 {
		return 1
	}
	return 0
}

// Uint64 compares the two values v1 and v2. Returns -1 if v1 < v2, 0 if v1 == v1, and +1 if v1 > v2.
func Uint64(v1 uint64, v2 uint64) int {
	if v1 < v2 {
		return -1
	} else if v1 > v2 {
		return 1
	}
	return 0
}

// IntSlice compares the two slices S1 and S2. Returns -1 if S1 < S2, 0 if S1 == S2, and +1 if S1 > S2. Order is grlex: if #S1 < #S2 then S1 < S2, if #S1 > #S2 then S1 > S2, otherwise the first entry such that S1[i] != S2[i] is used to determine the order.
func IntSlice(S1 []int, S2 []int) int {
	if len(S1) < len(S2) {
		return -1
	} else if len(S1) > len(S2) {
		return 1
	}
	for i, e1 := range S1 {
		e2 := S2[i]
		if e1 < e2 {
			return -1
		} else if e1 > e2 {
			return 1
		}
	}
	return 0
}

// Int8Slice compares the two slices S1 and S2. Returns -1 if S1 < S2, 0 if S1 == S2, and +1 if S1 > S2. Order is grlex: if #S1 < #S2 then S1 < S2, if #S1 > #S2 then S1 > S2, otherwise the first entry such that S1[i] != S2[i] is used to determine the order.
func Int8Slice(S1 []int8, S2 []int8) int {
	if len(S1) < len(S2) {
		return -1
	} else if len(S1) > len(S2) {
		return 1
	}
	for i, e1 := range S1 {
		e2 := S2[i]
		if e1 < e2 {
			return -1
		} else if e1 > e2 {
			return 1
		}
	}
	return 0
}

// Int16Slice compares the two slices S1 and S2. Returns -1 if S1 < S2, 0 if S1 == S2, and +1 if S1 > S2. Order is grlex: if #S1 < #S2 then S1 < S2, if #S1 > #S2 then S1 > S2, otherwise the first entry such that S1[i] != S2[i] is used to determine the order.
func Int16Slice(S1 []int16, S2 []int16) int {
	if len(S1) < len(S2) {
		return -1
	} else if len(S1) > len(S2) {
		return 1
	}
	for i, e1 := range S1 {
		e2 := S2[i]
		if e1 < e2 {
			return -1
		} else if e1 > e2 {
			return 1
		}
	}
	return 0
}

// Int32Slice compares the two slices S1 and S2. Returns -1 if S1 < S2, 0 if S1 == S2, and +1 if S1 > S2. Order is grlex: if #S1 < #S2 then S1 < S2, if #S1 > #S2 then S1 > S2, otherwise the first entry such that S1[i] != S2[i] is used to determine the order.
func Int32Slice(S1 []int32, S2 []int32) int {
	if len(S1) < len(S2) {
		return -1
	} else if len(S1) > len(S2) {
		return 1
	}
	for i, e1 := range S1 {
		e2 := S2[i]
		if e1 < e2 {
			return -1
		} else if e1 > e2 {
			return 1
		}
	}
	return 0
}

// Int64Slice compares the two slices S1 and S2. Returns -1 if S1 < S2, 0 if S1 == S2, and +1 if S1 > S2. Order is grlex: if #S1 < #S2 then S1 < S2, if #S1 > #S2 then S1 > S2, otherwise the first entry such that S1[i] != S2[i] is used to determine the order.
func Int64Slice(S1 []int64, S2 []int64) int {
	if len(S1) < len(S2) {
		return -1
	} else if len(S1) > len(S2) {
		return 1
	}
	for i, e1 := range S1 {
		e2 := S2[i]
		if e1 < e2 {
			return -1
		} else if e1 > e2 {
			return 1
		}
	}
	return 0
}

// UintSlice compares the two slices S1 and S2. Returns -1 if S1 < S2, 0 if S1 == S2, and +1 if S1 > S2. Order is grlex: if #S1 < #S2 then S1 < S2, if #S1 > #S2 then S1 > S2, otherwise the first entry such that S1[i] != S2[i] is used to determine the order.
func UintSlice(S1 []uint, S2 []uint) int {
	if len(S1) < len(S2) {
		return -1
	} else if len(S1) > len(S2) {
		return 1
	}
	for i, e1 := range S1 {
		e2 := S2[i]
		if e1 < e2 {
			return -1
		} else if e1 > e2 {
			return 1
		}
	}
	return 0
}

// Uint8Slice compares the two slices S1 and S2. Returns -1 if S1 < S2, 0 if S1 == S2, and +1 if S1 > S2. Order is grlex: if #S1 < #S2 then S1 < S2, if #S1 > #S2 then S1 > S2, otherwise the first entry such that S1[i] != S2[i] is used to determine the order.
func Uint8Slice(S1 []uint8, S2 []uint8) int {
	if len(S1) < len(S2) {
		return -1
	} else if len(S1) > len(S2) {
		return 1
	}
	for i, e1 := range S1 {
		e2 := S2[i]
		if e1 < e2 {
			return -1
		} else if e1 > e2 {
			return 1
		}
	}
	return 0
}

// Uint16Slice compares the two slices S1 and S2. Returns -1 if S1 < S2, 0 if S1 == S2, and +1 if S1 > S2. Order is grlex: if #S1 < #S2 then S1 < S2, if #S1 > #S2 then S1 > S2, otherwise the first entry such that S1[i] != S2[i] is used to determine the order.
func Uint16Slice(S1 []uint16, S2 []uint16) int {
	if len(S1) < len(S2) {
		return -1
	} else if len(S1) > len(S2) {
		return 1
	}
	for i, e1 := range S1 {
		e2 := S2[i]
		if e1 < e2 {
			return -1
		} else if e1 > e2 {
			return 1
		}
	}
	return 0
}

// Uint32Slice compares the two slices S1 and S2. Returns -1 if S1 < S2, 0 if S1 == S2, and +1 if S1 > S2. Order is grlex: if #S1 < #S2 then S1 < S2, if #S1 > #S2 then S1 > S2, otherwise the first entry such that S1[i] != S2[i] is used to determine the order.
func Uint32Slice(S1 []uint32, S2 []uint32) int {
	if len(S1) < len(S2) {
		return -1
	} else if len(S1) > len(S2) {
		return 1
	}
	for i, e1 := range S1 {
		e2 := S2[i]
		if e1 < e2 {
			return -1
		} else if e1 > e2 {
			return 1
		}
	}
	return 0
}

// Uint64Slice compares the two slices S1 and S2. Returns -1 if S1 < S2, 0 if S1 == S2, and +1 if S1 > S2. Order is grlex: if #S1 < #S2 then S1 < S2, if #S1 > #S2 then S1 > S2, otherwise the first entry such that S1[i] != S2[i] is used to determine the order.
func Uint64Slice(S1 []uint64, S2 []uint64) int {
	if len(S1) < len(S2) {
		return -1
	} else if len(S1) > len(S2) {
		return 1
	}
	for i, e1 := range S1 {
		e2 := S2[i]
		if e1 < e2 {
			return -1
		} else if e1 > e2 {
			return 1
		}
	}
	return 0
}

// Float32 compares the two values v1 and v2. Returns -1 if v1 < v2, 0 if v1 == v1, and +1 if v1 > v2.
func Float32(v1 float32, v2 float32) int {
	if v1 < v2 {
		return -1
	} else if v1 > v2 {
		return 1
	}
	return 0
}

// Float64 compares the two values v1 and v2. Returns -1 if v1 < v2, 0 if v1 == v1, and +1 if v1 > v2.
func Float64(v1 float64, v2 float64) int {
	if v1 < v2 {
		return -1
	} else if v1 > v2 {
		return 1
	}
	return 0
}

// Float32Slice compares the two slices S1 and S2. Returns -1 if S1 < S2, 0 if S1 == S2, and +1 if S1 > S2. Order is grlex: if #S1 < #S2 then S1 < S2, if #S1 > #S2 then S1 > S2, otherwise the first entry such that S1[i] != S2[i] is used to determine the order.
func Float32Slice(S1 []float32, S2 []float32) int {
	if len(S1) < len(S2) {
		return -1
	} else if len(S1) > len(S2) {
		return 1
	}
	for i, e1 := range S1 {
		e2 := S2[i]
		if e1 < e2 {
			return -1
		} else if e1 > e2 {
			return 1
		}
	}
	return 0
}

// Float64Slice compares the two slices S1 and S2. Returns -1 if S1 < S2, 0 if S1 == S2, and +1 if S1 > S2. Order is grlex: if #S1 < #S2 then S1 < S2, if #S1 > #S2 then S1 > S2, otherwise the first entry such that S1[i] != S2[i] is used to determine the order.
func Float64Slice(S1 []float64, S2 []float64) int {
	if len(S1) < len(S2) {
		return -1
	} else if len(S1) > len(S2) {
		return 1
	}
	for i, e1 := range S1 {
		e2 := S2[i]
		if e1 < e2 {
			return -1
		} else if e1 > e2 {
			return 1
		}
	}
	return 0
}

// Time compares the two values v1 and v2. Returns -1 if v1 < v2, 0 if v1 == v1, and +1 if v1 > v2.
func Time(v1 time.Time, v2 time.Time) int {
	if v1.Before(v2) {
		return -1
	} else if v1.After(v2) {
		return 1
	}
	return 0
}
